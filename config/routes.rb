Rails.application.routes.draw do
  devise_for :users

  root 'welcome#dashboard'

  post 'crawl' => 'welcome#start_crawl'
  get 'activity' => 'welcome#activity_feed'
  get 'activity/:id/results' => 'welcome#results', as: "results"

end
