class SnapdealScraper < Crawler::Base
  def crawl
    html_doc, crawl_detail = self.fetch_data
    arr = []
    
    data = html_doc.search('#products section > div .product-tuple-description').collect do |row|
      begin 
        title = row.xpath('div').search('.product-title').text if row.search('.product-title').present?
        rating = (row.xpath('div').search('.filled-stars').attr('style').value[/[0-9]+/].to_i/100.0)*5 if row.search('.filled-stars').present?
        orig_price = row.search('.product-desc-price').text[/[0-9]+(.)+[0-9]+/].gsub(",", "").to_f if row.search('.product-desc-price').present?
        dis_price =  row.search('.product-price').text[/[0-9]+(.)+[0-9]+/].gsub(",", "").to_f if row.search('.product-price').present?
        emi = row.search('.product-emi').text[/[0-9]+(.)+[0-9]+/].gsub(",","").to_f if row.search('.product-emi').present?
        arr << {'title': title, 'rating': rating, 'orig_price': orig_price, 'discount_price': dis_price, 'emi': emi, 'crawl_detail_id': crawl_detail.id } 
      rescue => e
        puts e.inspect
      end
    end
    self.save_product_details(crawl_detail.id, arr)
  end
end