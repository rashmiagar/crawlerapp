module Crawler
  class Base
    def initialize(user, url)
      @user = user
      @url = url
    end

    def fetch_data
      retry_count = 0
      html_doc = nil
      while(retry_count < 3)
        begin
          uri = URI.parse(@url)
          http = Net::HTTP.new(uri.host, uri.port)
          if(uri.port == 443)
            http.use_ssl = true
          end
          response = http.request(Net::HTTP::Get.new(uri.request_uri))
          html_doc = Nokogiri::HTML(response.body)
          break    
        rescue SocketError
            retry_msg = "Socket error"
            retry_count = retry_count + 1
        rescue TimeoutError
          retry_msg = "Timeout error"
          retry_count = retry_count + 1
        rescue => e
          return false
        end  
      end

      if retry_count >= 3 && html_doc.nil?
        puts "----->>>>> Fetching data failed after several retries <<<<<-----"
        return false
      end

      update_crawling_details

      return [html_doc, @obj]
    end

    def save_product_details(crawl_detail_id, details=[])
      ProductDetail.create(details)
      CrawlDetail.find(crawl_detail_id).update(no_of_records_crawled: details.size)
    end

    private

    def update_crawling_details
      @obj = CrawlDetail.new
      @obj.user_id = @user.id
      @obj.crawl_url = @url
      @obj.crawl_time = Time.now
      @obj.save
    end
  end
end