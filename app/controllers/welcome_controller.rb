
class WelcomeController < ApplicationController

  def dashboard
  	@activities = current_user.crawl_details.order("crawl_time DESC").limit(9)
  	@total_data_extracted = current_user.product_details.count
  	@total_url_crawled = current_user.crawl_details.count
  end

  def start_crawl
  	puts params[:url]
  	SnapdealScraper.new(current_user, params[:url]).crawl
    respond_to do |format|
      format.js {render js: "window.location.href='"+root_url+"'"}
    end
  end

  def activity_feed
	@activities = current_user.crawl_details
  	respond_to do |format|
      format.html
  	end
  end

  def results
  	@activity = CrawlDetail.find(params[:id])
  	@data = ProductDetail.where("crawl_detail_id=?", @activity.id)
    respond_to do |format|
      format.html
      format.csv do
        headers['Content-Disposition'] = "attachment; filename=\"product-details\""
        headers['Content-Type'] ||= 'text/csv'
      end
    end
  end
end