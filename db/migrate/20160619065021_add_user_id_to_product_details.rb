class AddUserIdToProductDetails < ActiveRecord::Migration
  def change
    add_column :product_details, :user_id, :integer
  end
end
