class ChangeRatingTypeInProductDetails < ActiveRecord::Migration
  def change
  	change_column :product_details, :rating, :float
  end
end
