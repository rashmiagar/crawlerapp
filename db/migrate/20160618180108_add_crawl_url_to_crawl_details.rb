class AddCrawlUrlToCrawlDetails < ActiveRecord::Migration
  def change
    add_column :crawl_details, :crawl_url, :string
  end
end
