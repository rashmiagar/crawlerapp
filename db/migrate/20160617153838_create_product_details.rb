class CreateProductDetails < ActiveRecord::Migration
  def change
    create_table :product_details do |t|
      t.integer :crawl_detail_id
      t.string :title
      t.integer :rating
      t.float :orig_price
      t.float :discount_price
      t.float :emi
      t.timestamps null: false
    end
  end
end
