class CreateCrawlDetails < ActiveRecord::Migration
  def change
    create_table :crawl_details do |t|
      t.integer :user_id
      t.datetime :crawl_time
      t.integer :no_of_records_crawled
      t.timestamps null: false
    end
  end
end
